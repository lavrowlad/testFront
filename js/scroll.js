$(document).ready(function(){
	$("#menu").on("click","a", function (event) {
		event.preventDefault();

		var id  = $(this).attr('href');
        var top = $(id).offset().top;
		
		$('body,html').animate({scrollTop: top}, 1500);
	});
    
    $("#go2top").on("click", function (event) {
		event.preventDefault();
        $('body,html').animate({scrollTop: 0}, 500);
    });
    
    
    $(window).scroll(function() {
        if( $(this).scrollTop() > 1 ){
            $($(".navigation")[0]).addClass('fixed');
            $($(".slider")[0]).css('marginTop', '72px');
        } else if( $(this).scrollTop() <= 1 ) {
            $($(".navigation")[0]).removeClass('fixed');
            $($(".slider")[0]).css('marginTop', '0px');
        }
        
        if( $(this).scrollTop() > 100 ) {
            $($('#go2top')).css('display', 'flex');
        } else {
             $($('#go2top')).css('display', 'none');
        }
    });
    
});